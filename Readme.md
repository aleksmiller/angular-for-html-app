# Создание простого приложения на Angular

Каталог товаров

## Установка

`git clone https://aleksmiller@bitbucket.org/aleksmiller/angular-for-html-workshop.git` клонируем репозиторий для локального использования

`npm run deps` ставим NPM и Bower пакеты

`npm run serve` запускаем Browser-Sync для просмотра и отслеживания изменений

## Задание

* Создать компонент каталога товаров, который будет доступен по url /catalogue

* Создать метод сервиса appHttp, который будет возвращать массив товаров из /api/catalogue.json

* Вывести список загруженных товаров в каталог (фото, название, цена), фото можно использовать свои

* Cоздать фильтр сортировки по имени и по цене