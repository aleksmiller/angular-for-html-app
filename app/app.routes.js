(function() {
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    function routeConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                template: '<div class="container module">' + 
                    '<h1>Hello everyone!</h1>' +
                '</div>'
            })

            .state('module', {
                url: '/module',
                templateUrl: 'app/components/module/module.html',
                controller: 'ModuleController',
                controllerAs: 'mc'
            })
            
            .state('about', {
                url: '/about',
                templateUrl: 'app/components/about/about.html'
            });
    }
})();